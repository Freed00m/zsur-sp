# Metodou k-means rozdělte všechna data do zjištěného počtu tříd
# - porovnejte nerovnoměrné bin. dělení s přímým dělením do cílového počtu tříd

import sys
sys.path.append('../custom/')
sys.path.append('../2_kmeans')
from load import *
import matplotlib.pyplot as plt
import random
from point import Point
from cluster import Cluster
import numpy as np
import math



def k_means(data = load_data_file(), R = 4, io_data='raw', static_data=False):

    if io_data == 'raw':
        data_objects = []
        for i in range(len(data)):
            data_objects.append( Point('x'+str(i), data[i]) )
    elif io_data == 'cluster':
        data_objects = data.get_points()
    else:
        data_objects = data

    pole_clusteru = []
    # 1.) Zvolíme R počátečních středů, mu_1, mu_2, ..., mu_R
    # (středy shluků lze vybrat uplně libovolně)

    staticke_pocatky = []
    if static_data:
        staticke_pocatky = [2,54,132,12,66,42,32] # statické počátky
    for i in range(R):
        if not static_data:
            staticke_pocatky.append(random.randint(0, len(data_objects)))
        pole_clusteru.append(Cluster( data_objects[ staticke_pocatky[i] ] ))



    # Splachni ty 4 body, aby tam nebyli
    for i in range(R):
        pole_clusteru[i].flush_all_points()

    pole_jakosti = [sys.maxsize]
    while(True):
        # print(len(pole_clusteru))
        # 2.) V k-tém iteračním kroku se rozdělují všechny obrazy množiny T do R
        #  shluků T_1, T_2, ..., T_R podle vztahu:
        #           x ∈ T_j(k) if d(x, mu_j(k)) = argmin d(x, mu_i(k))
        for k in range(len(data_objects)):
            min_vzdalenost = sys.maxsize
            index_clusteru_s_minimalni_vzdalenosti = -1
            for t in range(R):
                # print(str(pole_clusteru[t].get_center()) + '  ' + str(data[k]))
                curr_vzdalenost = fastest_calc_dist(pole_clusteru[t].get_center(), data_objects[k].get_position(), 'squared')
                if curr_vzdalenost < min_vzdalenost:
                    min_vzdalenost = curr_vzdalenost
                    index_clusteru_s_minimalni_vzdalenosti = t
            pole_clusteru[index_clusteru_s_minimalni_vzdalenosti].add_point(data_objects[k])

        for i in range(len(pole_clusteru)):
                pole_clusteru[i].calculate_center()
            # print(pole_clusteru[i].calculate_center())
        # print(pole_clusteru[0].get_center())
        # 3.) Z výsledku kroku 2 vypočti pro každý shluk nový střed tj mu_i(k+1)
        #        pro i = 1, ..., R tak, aby suma kvadrátů vzdáleností všech
        #  obrazů v T_j do nového středu byla minimální. Minimalizujeme tedy
        #  kriterium:        J_j(k+1) = ∑_{x ∈ T_j(k)} d^2 (x, mu_j(k+1))
        #  Lze určit ze vstahu
        #         mu_j(k+1) = 1 / s_j(k) ∑_{x ∈ T_j(k)} x
        #  A to platí pro všechna j = 1, ..., R
        celkova_jakost = -1
        for j in range(R):
            jakost_jednoho_clusteru = -1
            for i in range(len(pole_clusteru[j])):
                jakost_jednoho_clusteru += fastest_calc_dist(pole_clusteru[j][i].get_position(), pole_clusteru[j].get_center())
            celkova_jakost += jakost_jednoho_clusteru
        pole_jakosti.append(celkova_jakost)

        print('J predchozi: \t' + str(pole_jakosti[-2]) + '\t J aktualni: ' + str(pole_jakosti[-1]))

        # 4.) Jestliže mu_j(k+1) = mu_j (k) pro všechna j, tak algoritmus
        #  dokonvergoal jinak jdi na krok 2 (Alternativně lze algoritmus ukončit
        #  i při nevýznamném poklesu kriteriální funkce)
        if round(pole_jakosti[-2], 6) == round(pole_jakosti[-1], 6):
            return [pole_clusteru, pole_jakosti[-1]]
            break

        # Musim vyhodit všechny přiřazený body z clusterů, zatímco, střed zůstane zachován
        for i in range(R):
            pole_clusteru[i].flush_all_points()

def k_means_nerovnomerne_bin_deleni(data = load_data_file(), R = 4):
    [pole_clusteru, vychozi_jakost] = k_means(data , 1)

    while(True):
        nejvetsi_jakost = -1
        index_nejvetsi_zkresleni = -1
        # získej Jakost
        for i in range(len(pole_clusteru)):
            jakost_jednoho_clusteru = 0
            for j in range(len(pole_clusteru[i])):
                jakost_jednoho_clusteru += fastest_calc_dist(pole_clusteru[i][j].get_position(), pole_clusteru[i].get_center())
            if nejvetsi_jakost < jakost_jednoho_clusteru:
                nejvetsi_jakost = jakost_jednoho_clusteru
                index_nejvetsi_zkresleni = i

        # V tomto kroce je nutné popnout ten cluster s největší jakosti,
        #  vytáhnout ty body a rozdělit k_means() na 2 a vložit zpátky

        popnuty_cluster = pole_clusteru.pop(index_nejvetsi_zkresleni)
        [dva_clustery, jakost_nepotrebujes]=k_means(popnuty_cluster, 2,io_data='cluster')
        pole_clusteru += dva_clustery

        # Po dosažení požadovaného počtu clusterů, ukonči algoritmus
        if len(pole_clusteru) == R:
            return pole_clusteru
            break

if __name__ == "__main__":

    [pole_clusteru, q] = k_means(static_data=True)
    # [pole_clusteru, q] = k_means()
    # pole_clusteru = k_means_nerovnomerne_bin_deleni()



    # pole_clusteru = k_means_nerovnomerne_bin_deleni()
    # print(pole_clusteru)
    for i in range(len(pole_clusteru)):
        plt.scatter(*zip(*pole_clusteru[i].get_points()),c=pole_clusteru[i].get_color(),marker='o' , s = 40, lw = 0)

    for i in range(len(pole_clusteru)):
        pole_clusteru[i].calculate_center()
        plt.annotate(
            str(i)+'. stredni hodnota',
            xy = pole_clusteru[i].get_center(), xytext = (-20, 20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

    plt.show()
