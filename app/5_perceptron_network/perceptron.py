# Na základě informace z prvního kroku nerovnoměrného binárního dělení viz bod 2) (pouze 2 třídy) natrénujte jednovrstvou perceptronovou neuronovou síť (která bude mezi těmito dvěma třídami rozhodovat).

# Intro to Neural networks
# Part 1: Data + Architecture     https://www.youtube.com/watch?v=bxe2T-V8XRs
# Part 2: Forward Propagation     https://www.youtube.com/watch?v=UJwK6jAStmg
# Part 3: Gradient Descent        https://www.youtube.com/watch?v=5u0jaA3qAGk
# Part 4: Backpropagation         https://www.youtube.com/watch?v=GlcnxUlrtek
# Part 5: Num. Gradient Check     https://www.youtube.com/watch?v=pHMzNW8Agq4
# Part 6: Training                https://www.youtube.com/watch?v=9KM9Td6RVgQ
# Part 7: Overfit, Test, and Reg. https://www.youtube.com/watch?v=S4ZUwgesjS8


import sys
sys.path.append('../custom/')
sys.path.append('../2_kmeans')
from load import *
import matplotlib.pyplot as plt
import random
from rozdelte_do_trid import k_means
from point import Point
from cluster import Cluster
import numpy as np
import math


class Neural_Network(object):
    def __init__(self):
        # 1.) Inicializace
        # Váhovou matici w(1) typu (m x n) prahový vektor b(1) dimenze
        #       inicializujeme malými náhodnými čísly (doporučení < -1, 1 >)
        #       Zvolíme konstantu učení     c > 0
        #       Spočteme E chybu způsobenou nesrávnou čiností sítě během jednoho
        #       trénovacího cyklu   q = 1 počet trénovacích cyklů
        # Hyperparametry, statické!
        self.inputLayerSize  = 2    # [x a y]
        self.outputLayerSize = 1    # trida 1 ci 2
        self.hiddenLayerSize = 1    # 1 stupne volnosti ? jedna přímka
        # Váhy (parametry), první, naprosto náhodné
        self.W = np.random.randn(self.inputLayerSize, self.hiddenLayerSize)
        self.b = np.random.randn(self.hiddenLayerSize, self.outputLayerSize)
        # Konstanta učení
        self.c = 0.01

    def forward(self, X):
        # 2.) Výpočet odezvy
        #       x(k) = x_p
        #       y(k) = [ sgn( w_1 x(k)  +  b_1(k) );
        #                sgn( w_2 x(k)  +  b_2(l) );
        #                              ...
        #                sgn( w_m x(k)  +  b_m(k) ) ].T
        #       u(k) = u_p
        #       kde w_i je i-tá řádka matice w
        #       b_i je i-tý prvek vektoru b
        # Propagace skrze síť TODO vyhrát si z transpozicí
        self.z2 = np.dot(X, self.W)         # před hiddenLayer
        self.a2 = self.z2 + self.b          # po hiddenLayer na
        y_i = np.sign(self.a2)
        return y_i

    def costFunction(self, X, u_p):
        # 3.) Výpočet chyby
        #   E(k) = E(k-1) + 1/2*(u(k) - y(k)).T (u(k) - y(k))
        # Compute cost for given X,y, use weights already stored in class.
        self.y_i = self.forward(X)
        J = 0.5*sum(np.dot((u_p-self.y_i).T,(u_p-self.y_i)))
        return J

    def recalculateWeights(self, U_k, Y_k, X):
        # 4.) Aktualizace vah a prahů
        #       w(k+1) = w(k) + c ∆w(k) = w(k) + c(u(k) - y(k)) x(k).T
        #       b(k+1) = b(k) + c ∆b(k) = b(k) + c(u(k) - y(k)) 1
        one = '\nVáhy W před: \n'+ str(self.W)
        self.W = self.W + self.c * np.dot((U_k - Y_k).T, X).T
        two = '\nVáhy W po: \n'+ str(self.W) + '\nVáhy b před:  '+ str(self.b)
        self.b = self.b + self.c * np.mean((U_k - Y_k).T, axis=1)
        three = '\nVáhy b po: '+ str(self.b) +'\n'
        # print(one+two+three)


def perceptron(data = load_data_file(), q = 1):
    [pole_clusteru, vychozi_jakost] = k_means(data, 2)
    NN = Neural_Network()

    X   = []
    U_p = []
    for k in range(len(pole_clusteru)):
        if k == 0:
            u_p = [1]
        else:
            u_p = [-1]
        for j in range(len(pole_clusteru[k])):
            pole_clusteru[k][j].set_u_p(u_p) # prvni hodnoty u_p do clusterů
            X.append(pole_clusteru[k][j].get_position())
            U_p.append(pole_clusteru[k][j].get_u_p())

    E_c = sys.maxsize
    while(E_c != 0):
        NN.recalculateWeights(U_k=np.array(U_p), Y_k=NN.forward(X) , X=np.array(X))
        E_c = NN.costFunction( X=X,  u_p=U_p )
        # 5.) Test vyčerpanosti trénovací množindexy
        #       Je-li p < P     pak     p = p + 1
        #                               k = k + 1
        #                               jdi na krok 2.)
        #                       jinak   q = q + 1
        #                               jdi na krok 6.)
        print(str(q)+'. iterace, Síť se učí, aktuální chyba: '+str(E_c))
        q += 1 # Jenom referenční proměná krok q

        # 6.) Konec trénovacího cyklu
        #       E_c(q) = E(k)
        # while(E_c   !=  0):
        # Je-li E_c(q) = 0    pak     potom konec!!
        #                     jinak   E(k) = 0
        #       k = k + 1
        #       p = 1
        #       jdi na krok 2.)

    for i in range(len(pole_clusteru)):
        plt.scatter(*zip(*pole_clusteru[i].get_points()),c=pole_clusteru[i].get_color(),marker='o' , s = 40, lw = 0)

    # Jelikož absolutně proboha netušim jak vykreslit rovnou čáru !!!!!
    k = -NN.W[0]/NN.W[1]
    b =  -NN.b[0]/NN.W[1]
    x1 = 20
    x2 = -20
    y1 = k * x1 + b
    y2 = k * x2 + b
    body = []
    plt.plot([x1,x2],[y1,y2], 'k-', lw=2)
    plt.axis(calculate_plot_space(data))
    plt.show()

        # Pokud lze natrénovat tak v konečném počtu kroků.
        # Vhodné omezit maximální počet kroků
        # Konec taky při malé změně E_c
        # Použití výhradně jako lineární klasifikátor

if __name__ == "__main__":
    np.set_printoptions(threshold=np.nan)
    perceptron()
    # perceptron(load_data_file('../source/data_michael.txt'))
