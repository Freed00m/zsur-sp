# Na základě informace od učitele (informace o zařazení do tříd ω i z bodu 2 popřípadě informace z bodu 3) natrénujte

# porovnejte potřebný počet iterací při použití Rosemblatova alg., metody konstantních přírůstků a upravené metody konstantních přírůstků pro několik zvolených konstant učení - podobně jako v předchozím bodě zakreslete pomocí rastru body v prostoru (trénovacích dat) které klasifikujeme do jednotlivých tříd a které případně klasifikovat nemůžeme (diskutujte proč)

import sys
sys.path.append('../custom/')
sys.path.append('../2_kmeans')
from load import *
import matplotlib.pyplot as plt
import random
import itertools
from rozdelte_do_trid import k_means_nerovnomerne_bin_deleni
from point import Point
from cluster import Cluster
import numpy as np
import math


def lin_discrimination(data = load_data_file(), classes=4, max_attempts =500, method='rosemblat', β = 0.5, static_data=False):

    centroid = [0, 0]
    centroid = calculate_centroid(data)
    if static_data:
        [pole_clusteru, q] = k_means(data, classes,static_data=True)
    else:
        pole_clusteru = k_means_nerovnomerne_bin_deleni(data, classes)

    # tato proměná se přepne ve chvíly, kdy data jsou neseparabilní
    has_to_run_different_thing = False

    # Vytvořit jemný rastr
    pole_random_bodu = []
    density = 50
    area = calculate_plot_space(data)
    # print(fastest_calc_dist([area[0], area[1]], [area[1], area[2]]))
    range_raster = (max(area[3],area[2]) + max(area[1],area[0])) *1.3
    for i in range(density):
        for j in range(density):
            pole_random_bodu.append([(i/density)*range_raster -(range_raster/2) +centroid[0],(j/density)*range_raster-(range_raster/2)+centroid[1]])

    #                   ROSEMBLAT
    # pro c_k = 1  a  δ = 0 dostaneme
    #           / q(k)                  je-li q.T(k)x(k+1)Ω(k+1) ≥ 0
    # q(k+1) =  \ q(k) + x(k+1)Ω(k+1)   je-li q.T(k)x(k+1)Ω(k+1) < 0
    # Pro linearně separabilní třídy se průběh diskriminační funkce velmi
    # výrazně mění, ale její schopnost správně klasifikovat je zachována.
    # Pokud nejsou třídy lineárně separovatelné nemusí algoritmus konvergovat
    # nebo nebude výsledek správně oddělovat obě třídy.
    #                   METODA KONSTANTNICH PRIRUSTKU
    # pro c_k = β / || x(k) ||^2 , β > 0 platí
    # pro c_k = 1  a  δ = 0 dostaneme
    #           / q(k)                      je-li q.T(k)x(k+1)Ω(k+1) ≥ 0
    # q(k+1) =  \ q(k) + c_k x(k+1)Ω(k+1)   je-li q.T(k)x(k+1)Ω(k+1) < 0
    # parametr c_k tlumí přírůstek, což má za následek zvýšení počtu iteračních kroků k dosažení správné diskriminační fce.
    #                   METODA KONSTANTNICH PRIRUSTKU (UPRAVENÁ)
    # Problém předešlé metody je to, že ani po změně q(k+1) nemusíme x(k+1) klasifikovat správně. Řešením je připočítat vektor
    #           (β / || x(k) ||^2)  * x(k+1)Ω(k+1)
    # tolikrát, až pro obraz x(k+1) dosáhneme správné Klasifikace
    #           q.T (k+1) x(k+1) Ω(k+1) ≥ 0
    # Platí tedy
    #           c_{k+1} = (β b_{k+1}) / || x(k+1) ||^2
    # kde b_{k+1} je nejmenší přirozené číslo splňující vztah
    #           b_{k+1} < (δ - q.T (k+1) x(k+1) Ω(k+1))  /  β

    primky = []
    op = 0
    for _ in range(classes): # proiterovat všechny body pro každou přímku zvláť
        q = np.array([[1],[1],[1]]) # první přímka je [1, 1, 1].
        rozhodnuti = True
        loop_timeout = False
        iteration_per_line = 0
        print('Upravuji přímku '+ str(_))
        while(rozhodnuti):
            rozhodnuti = False
            iteration_per_line += 1 # count loop interation per line
            for j in range(len(pole_clusteru)):
                if _ == j: Ω =  1 # přímka 0 rozděluje 0tý cluster od zbytku, atd...
                else:      Ω = -1
                for i in range(len(pole_clusteru[j])):
                    op += 1
                    mon = pole_clusteru[j][i].get_position()
                    x = np.array([[1],[mon[0]-centroid[0]], [mon[1]-centroid[1]]])
                    if iteration_per_line >= max_attempts:
                        loop_timeout = True
                    if np.dot(q.T, x)*Ω < 0 and not loop_timeout:
                        if method == 'equal_intervals_enhanced':
                            c_k = β / math.sqrt( x[1]**2 + x[2]**2 )
                            optimum = False
                            while (not optimum):
                                q = q + c_k * x * Ω
                                if np.dot(q.T, x)*Ω >= 0:
                                    optimum = True
                        elif method == 'equal_intervals':
                                c_k = β / math.sqrt( x[1]**2 + x[2]**2 )
                                q = q + c_k * x * Ω
                        else: # rosemblat is default behavior
                            q = q + x * Ω
                        rozhodnuti = True # loop pokračuje pokud se sem dostane
        print('Přímka '+str(_)+' potřebovala '+str(iteration_per_line)+' iterací')
        iteration_per_line = 0 # Reset iteration counter
        if not loop_timeout:
            primky.append(q)
        else:
            print('Přímka nebyla nalezena po více než '+str(max_attempts)+' průchodech, předpokládám linearní neseparabilitu')
            has_to_run_different_thing = True
    print('Celkový počet operací '+str(op)+ ' hodnota β = '+str(β))

    if has_to_run_different_thing:
        print('Jelikož se předchozí pokus tak úplně nezdařil (né čistě separabilní data), \n spustím teď jiný algoritmus :O \nTeď dělíš dvojce klusterů, a rozhoduješ relevatnímy maskami s a relevatnimy přímkami\n\n')

        # Potřebuješ veškeré dvojce clusterů
        primky = []
        kombi = list(itertools.combinations(range(len(pole_clusteru)), 2))
        for _ in range(int(classes * (classes-1)/2)): # proiterovat všechny body pro každou přímku zvlášť
            q = np.array([[1],[1],[1]]) # první přímka je [1, 1, 1].
            rozhodnuti = True
            loop_timeout = False
            iteration_per_line = 0
            print('Upravuji přímku '+ str(_))
            while(rozhodnuti):
                rozhodnuti = False
                iteration_per_line += 1 # count loop interation per line
                for j in range(len(pole_clusteru)):
                    skip_this_cluster = False
                    if j == kombi[_][0]: Ω =  1 # přímka 0 rozděluje dvojici clusterů od zbytku, atd, proiterovat kombinace
                    elif j == kombi[_][1]: Ω = -1
                    else:      Ω = 0
                    for i in range(len(pole_clusteru[j])):
                        op += 1
                        mon = pole_clusteru[j][i].get_position()
                        x = np.array([[1],[mon[0]-centroid[0]], [mon[1]-centroid[1]]])
                        # x = pole_clusteru[j][i].get_position()
                        if iteration_per_line >= max_attempts:
                            loop_timeout = True
                        if np.dot(q.T, x)*Ω < 0 and not loop_timeout:
                            if method == 'equal_intervals_enhanced':
                                c_k = β / math.sqrt( x[1]**2 + x[2]**2 )
                                optimum = False
                                while not optimum:
                                    q = q + c_k * x * Ω
                                    if np.dot(q.T, x)*Ω >= 0:
                                        optimum = True
                            elif method == 'equal_intervals':
                                    c_k = β / math.sqrt( x[1]**2 + x[2]**2 )
                                    q = q + c_k * x * Ω
                            else: # rosemblat is default behavior
                                q = q + x * Ω
                            rozhodnuti = True # loop pokračuje pokud se sem dostane
            print('Přímka ' + str(_) + ' potřebovala ' + str(iteration_per_line)+' iterací')
            iteration_per_line = 0 # Reset iteration counter

            primky.append(q) # legacy přímky, pro ploting

            # Z protože oddělujeme pouze dvojce, ulož přímku jen k těm třídám
            print('Přidávám do clusterů '+str(kombi[_]))
            pole_clusteru[kombi[_][0]].add_primka(q, centroid)
            pole_clusteru[kombi[_][1]].add_primka(q, centroid)


    if not has_to_run_different_thing: # pokud byly shluky separabilní
        pole_masek = [] # jelikož proboha neznám orientaci klasifikačního vektoru :O
        for i in range(len(pole_clusteru)):
            x = pole_clusteru[i][0].get_position()
            maska = []
            for j in range(len(primky)):
                if x[1] - centroid[1] > (primky[j][0] + primky[j][1] * (x[0]- centroid[0])) / - primky[j][2]:
                    maska.append(1)
                else:
                    maska.append(0)
            pole_masek.append(maska)

        for i in range(len(pole_random_bodu)):
            curr_maska = []
            for c in range(len(primky)):
                if pole_random_bodu[i][1] - centroid[1] > (primky[c][0] + primky[c][1] * (pole_random_bodu[i][0] - centroid[0])) / - primky[c][2]:
                    curr_maska.append(1)
                else:
                    curr_maska.append(0)
            for j in range(len(pole_masek)):
                if pole_masek[j] == curr_maska:
                    plt.scatter(*pole_random_bodu[i], marker='+', s=30 , c=pole_clusteru[j].get_color(), lw = 2)

    if has_to_run_different_thing:

        # print(len(pole_random_bodu))
        for i in range(len(pole_random_bodu)):  # prvně vem bod z rastru
            for c in range(len(pole_clusteru)): # projeď všechny clustery
                cluster_maska = pole_clusteru[c].get_maska()
                curr_relevant = pole_clusteru[c].get_primky()
                curr_maska = []
                # print(len(curr_relevant))
                for j in range(len(curr_relevant)):
                    if pole_random_bodu[i][1] - centroid[1] > (curr_relevant[j][0] + curr_relevant[j][1] * (pole_random_bodu[i][0] - centroid[0])) / - curr_relevant[j][2]:
                        curr_maska.append(1)
                    else:
                        curr_maska.append(0)
                # print(curr_maska)
                if curr_maska == cluster_maska:
                    plt.scatter(*pole_random_bodu[i], marker='+', s=30 , c=pole_clusteru[c].get_color(), lw = 2)
                    break # už není potřeba testovat dál, vyhovuje masce

    plt.axis(area)
    for i in range(len(pole_clusteru)):
        plt.scatter(*zip(*pole_clusteru[i].get_points()),c=pole_clusteru[i].get_color(),marker='o' , s = 40, lw = 2)

    for i in range(len(primky)):
        # g(x) = 0  -> y = (q2 x + q1)/(-q3)
        x1 = 2000
        x2 = -2000
        y1 = (-primky[i][1]/primky[i][2]) * x1 - primky[i][0]/primky[i][2]
        y2 = (-primky[i][1]/primky[i][2]) * x2 - primky[i][0]/primky[i][2]
        plt.plot([x1+centroid[0],x2+centroid[0]],[y1+centroid[1],y2+centroid[1]], 'k-', lw=2,c='r')
    plt.show()

def calculate_centroid(data):
    a = list(zip(*data))
    return [sum(a[0])/len(data),  sum(a[1])/len(data) ]

if __name__ == "__main__":


    lin_discrimination(method='rosemblat')
    # lin_discrimination(method='equal_intervals')
    # lin_discrimination(method='equal_intervals_enhanced')
    # lin_discrimination(method='rosemblat',static_data=True)
    # lin_discrimination(method='equal_intervals',static_data=True)
    # lin_discrimination(method='equal_intervals_enhanced',static_data=True)


    # Kolega michael
    # lin_discrimination(data = translate_data(load_data_file('../source/data_michael.txt')), classes=3, max_attempts =500, method='rosemblat')
    # lin_discrimination(data = load_data_file('../source/data_michael.txt'), classes=3, max_attempts =500, method='rosemblat')
    # lin_discrimination(data = load_data_file('../source/data_michael.txt'), classes=3, max_attempts =500, method='equal_intervals',β = 0.5)
    # lin_discrimination(data = load_data_file('../source/data_michael.txt'), classes=3, max_attempts =500, method='equal_intervals_enhanced',β = 0.5)
