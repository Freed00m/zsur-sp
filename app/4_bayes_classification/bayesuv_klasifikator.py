# Na základě informace od učitele (informace o zařazení do tříd ω i z bodu 2 popřípadě informace z bodu 3) natrénujte
import sys
sys.path.append('../custom/')
sys.path.append('../2_kmeans')
from load import *
import matplotlib.pyplot as plt
import random
from rozdelte_do_trid import k_means_nerovnomerne_bin_deleni
from rozdelte_do_trid import k_means
from point import Point
from cluster import Cluster
import numpy as np
import math
from scipy.stats import multivariate_normal

# Bayesův klasifikátor - tady nepředpokládám explicitně řešení té hranice (kuželosečky) stačí odhadnout parametry normálních rozložení a nějakým dostatečně jemným rastrem ohodnotit body v prostoru (kde se vyskytují trénovací data) kam který bod má největší pravděpodobnost.
def bayesuv_klasifikator(data = load_data_file(), classes= 4, static_data=False):
    if static_data:
        [pole_clusteru , q ] = k_means(static_data=True)
    else:
        pole_clusteru = k_means_nerovnomerne_bin_deleni(data, R=classes)
    #  1.) Spočtení podmíněné (aposteriorní) ppsti P( ω_r | x_i ) pro ∀x_i ∈ R
    pole_Σ = []
    pole_μ = []
    pole_random_bodu = []

    # Vytvořit jemný rastr
    density = 100
    range_raster = 60
    for i in range(density):
        for j in range(density):
            pole_random_bodu.append([(i/density)*range_raster -(range_raster/2),(j/density)*range_raster-(range_raster/2)])

    # μ = 1 / N  Σ x_i
    N = len(data)
    for i in range(len(pole_clusteru)):
        pole_μ.append(np.mean(pole_clusteru[i].get_matrix(),axis=0)) # mean hod.

    for i in range(len(pole_clusteru)):
        sumarum = np.zeros(shape=(2,2))
        for j in range(len(pole_clusteru[i])):
            Xi = np.array([pole_clusteru[i][j].get_position()])
            μ  = pole_μ[i]
            sumarum = sumarum + np.dot((Xi - μ).T,(Xi - μ))
        pole_Σ.append((1/len(pole_clusteru[i]))*sumarum) # Kovarianční matice

    pole_velikosti = []
    for i in range(len(pole_clusteru)):  # tady se zbavíš těch nacvičených bodů
        pole_velikosti.append(len(pole_clusteru[i])) # potřebuješ pro P( ω_r )
        pole_clusteru[i].flush_all_points()

    #  2.) Nalezení maxima P( ω_r | x ) ∀x ∈ R
    for i in range(len(pole_random_bodu)):
        my_list = []
        for j in range(len(pole_clusteru)):
            #  argmax P( ω_r | x ) = argmax P( x | ω_r ) P( ω_r )
            my_list.append(multivariate_normal.pdf(x=np.array([pole_random_bodu[i]]), mean=pole_μ[j], cov=pole_Σ[j]) * (pole_velikosti[j]/N)   )
        val, idx = max((val, idx) for (idx, val) in enumerate(my_list))
        #  3.) Klasifikace x do nalezené třídy
        if val != float("inf"):
            pole_clusteru[idx].add_point(Point('x'+str(i), pole_random_bodu[i]))

    # plt.axis(calculate_plot_space(data))
    plt.axis([-30, 30, -30, 30])
    for i in range(len(pole_clusteru)):
        plt.scatter(*zip(*pole_clusteru[i].get_matrix()),c=pole_clusteru[i].get_color(),marker='o' , s = 40, lw = 0)
    plt.scatter(*zip(*data), marker='.' , s = 40, lw = 1)

    plt.show()

# def norm_pdf_multivariate(x, μ, Σ):
#     size = len(x)
#     if size == len(μ) and (size, size) == Σ.shape:
#         det = np.linalg.det(Σ)
#         if det == 0:
#             raise NameError("The covariance matrix can't be singular")
#
#         norm_const = 1.0/ ( math.pow((2*3.14),float(size)/2) * math.pow(det,1.0/2) )
#         x_μ = np.matrix(x - μ)
#         inv = Σ**(-1)
#         result = np.exp(-0.5 * (x_μ.T * inv * x_μ))
#         return norm_const * result
#     else:
#         raise NameError("The dimensions of the input don't match")


if __name__ == "__main__":
    # bayesuv_klasifikator(data = load_data_file('../source/data_tomas.txt'), classes= 3)

    # bayesuv_klasifikator()
    bayesuv_klasifikator(static_data=True)
