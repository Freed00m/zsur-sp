# Určete počet tříd
# volte různé hladiny
# TODO metody vzájemně porovnejte.

import sys
sys.path.append('../custom/')
from load import *
import matplotlib.pyplot as plt
import random
from point import Point
from cluster import Cluster
import numpy as np
import math


def shlukova_hladina(data = load_data_file(), PRAH_VZDALENOSTI = 1.1):
# Aglomerativní shluková metoda, v každém iteračním kroku, budeme spojovat pouze
# 2 třídy, co mají k sobě nejblíže.
#           1.) Co dato, jedna třída, podobnost je na začátku nulová
#           2.) V ktém kroku, vybereme nejbližší třídy.
#           3.) sjednotíme
# x1 = Point('x1', [-3,  1])

    # Vytvoř pole clusterů
    pole_clusteru = []
    for i in range(len(data)):
        point_name = 'x'+str(i)
        pole_clusteru.append(Cluster(Point(point_name, data[i])))

    # Musíme vypočítat vzdálenost ke všem třídám
    matice_vzdalenosti = np.array(create_range_matrix_from_clusters(pole_clusteru))
    # print(matice_vzdalenosti)

    # Nevytvářet novou matici vzdálenosti, stačí umazat sloupec a řádek,
    # těch tříd, co to vcuclo

    # Porovnání dvou třidy s třídou, uděláš tak, že vemeš minimum  z těch všech
    #  vzdáleností všech bodů. Resp. budeš počitat všechny body se všema a
    #  vybereš tu nejmenší vzdálenost a narveš do tabulky.
    vzdalenost_nejblizsich_trid = -1
    while(True):
        # třídy ke sloučení, funkce najde nejlbizsi sousedy a vrati jejich index
        nejblizsi_tridy = najit_index_dalsi_nejblizsisousedy(matice_vzdalenosti)
        index_prvni_tridy = nejblizsi_tridy[0][0]
        index_druhe_tridy = nejblizsi_tridy[0][1]
        vzdalenost_nejblizsich_trid = nejblizsi_tridy[1]
        print('Clusterů: ' + str(len(pole_clusteru)) + ' a nejbližší třídy jsou vzdálené ' + str(vzdalenost_nejblizsich_trid) + ' jednotek')
        # print('Aktuální vzdálenost nejbližších clusterů: '+ str(vzdalenost_nejblizsich_trid))
        if vzdalenost_nejblizsich_trid > PRAH_VZDALENOSTI or len(matice_vzdalenosti) == 2:
            break

        # merdžne dva clustery k sobě
        cluster1 = pole_clusteru.pop(index_prvni_tridy)
        cluster2 = pole_clusteru.pop(index_druhe_tridy - 1) # minus jeden, už si popnul přece
        cluster1.merge_with(cluster2) # první poplý cluster vcucne ten druhý
        pole_clusteru.append(cluster1) # zpětně přidej ten cluster

        # Zapamatuj si ty radky pred smaznim
        radekA = matice_vzdalenosti[nejblizsi_tridy[0][0]]
        radekB = matice_vzdalenosti[nejblizsi_tridy[0][1]]
        radekA = np.delete(radekA, nejblizsi_tridy[0])
        radekB = np.delete(radekB, nejblizsi_tridy[0])

        # Vymaž dvě třídy z matice vzdalenosti
        matice_vzdalenosti = np.delete(matice_vzdalenosti, nejblizsi_tridy[0], axis=0)
        matice_vzdalenosti = np.delete(matice_vzdalenosti, nejblizsi_tridy[0], axis=1)
        # Vytvoř nový sloupec / řádek pro matice_vzdalenosti
        # a dopočti vzdalenosti
        new_distances = []
        for i in range(len(pole_clusteru) - 1):
            new_distances.append(min(radekA[i], radekB[i]))

        # přidat nový sloupec zpět do matice vzdálenosti
        # TODO naučit se to nějak civilizovaně, tohle je total CRAP!!
        b = np.array([new_distances])
        matice_vzdalenosti = np.concatenate((matice_vzdalenosti, b.T), axis=1)
        new_distances.append(0) # přidat 0 na konec
        matice_vzdalenosti = np.concatenate((matice_vzdalenosti, [new_distances]), axis=0)
        # print(matice_vzdalenosti)

    plt.axis(calculate_plot_space(data))
    # plt.scatter(*zip(*data))
    for i in range(len(pole_clusteru)):
        plt.scatter(*zip(*pole_clusteru[i].get_points()),c=pole_clusteru[i].get_color(),marker='o' , s = 40, lw = 0)
        pole_clusteru[i].calculate_center()
        plt.annotate(
            str(i)+'. stred',
            xy = pole_clusteru[i].get_center(), xytext = (-20, 20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    plt.show()

def najit_index_dalsi_nejblizsisousedy(matice_vzdalenosti = []):
    helper = sys.maxsize
    kandidati = []
    for i in range(len(matice_vzdalenosti)):
        for j in range(len(matice_vzdalenosti[0])-i-1):
            vzdalenost_mezi_body = matice_vzdalenosti[i][i+j+1]
            if vzdalenost_mezi_body < helper:
                helper = vzdalenost_mezi_body
                kandidati = [ i, i+j+1 ]
    return [kandidati, helper]

if __name__ == "__main__":
    # print(vzdalenost_mezi_dvema_tridama([[5,5], [6, 8]], [[6, 9]]))
    shlukova_hladina()
