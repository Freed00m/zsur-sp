# Určete počet tříd
# TODO metody vzájemně porovnejte.

import sys
sys.path.append('../custom/')
from load import *
import matplotlib.pyplot as plt
import random
from point import Point
from cluster import Cluster
import numpy as np
import math


# Jedná se o jednoduchý heuristický algoritmus, který je velmi vhodný pro určení
# (odhad) počtu shluků v obrazovém prostoru. Předpokládejme, že je dána množina
# obrazů T = {x1, x2, x3, ... , xN } a volitelná konstanta q

def maxmin(data = load_data_file(), q = 0.6):

    pole_clusteru = []

    #  1.) Z trénovací množiny vybereme libovolný počáteční ("startovací")
    # obraz a označímeho x_(1) a ztotožníme ho se středem prvního shluku, "mu_1"
    nahodny_pocatek = random.randint(0, len(data))
    # nahodny_pocatek = 17

    pole_clusteru.append(Cluster(Point('x'+str(nahodny_pocatek), data.pop(nahodny_pocatek))))

    #  2.) Dále určíme nejvzdálenější (ve smyslu zvolené metriky) obraz
    #  k obrazu mu_1 a označíme ho x_(2) a uděláme z něj střed mu_2
    mu_1 = pole_clusteru[0][0]
    index_nejvzdalenejsiho_bodu = -1
    maxi_distance = 0
    for i in range(len(data)):
        curr_distance = fastest_calc_dist(mu_1.get_position(), data[i])
        if curr_distance > maxi_distance:
            maxi_distance = curr_distance
            index_nejvzdalenejsiho_bodu = i
    print('Maximální vzdálenost obrazu k μ1 je ' + str(maxi_distance))

    pole_clusteru.append(Cluster(Point('x'+str(index_nejvzdalenejsiho_bodu), data.pop(index_nejvzdalenejsiho_bodu))))
    mu_2 = pole_clusteru[1][0]

    while(True):
        #  3.) Pro každý obraz trénovací množiny (kromě x_(1) a x_(2) )
        #  vyčíslíme vzdálenost k mu_1 a mu_2 a z každé dvojice uchováme tu min
        pole_minimalnich_vzdalenosti = []
        for i in range(len(data)):
            curr_pole_vzdalenosti = []
            for j in range(len(pole_clusteru)):
                curr_pole_vzdalenosti.append(fastest_calc_dist(data[i], pole_clusteru[j][0].get_position()))
            pole_minimalnich_vzdalenosti.append(min(curr_pole_vzdalenosti))

        #  4.) Z uchovaných minimálních vzdáleností vyberme maximální a
        #  porovnáme její velikost velikostí vzdáleosti q*d(mu_1,mu_2).
        #  Dále určíme odpovídající obraz trénovací množiny x_(3).
        max_hodnota_vsech_minim = max(pole_minimalnich_vzdalenosti)
        index_max = np.array(pole_minimalnich_vzdalenosti).argmax() #  odpovídající index obrazu trénovací množiny x_(3)

        #   5.) Jestliže je maximální vzdálenost větší než q*d(mu_1,mu_2),
        #  zavedeme nový shluk se středem mu_3, jinak můžeme skončit.

        if max_hodnota_vsech_minim > q*maxi_distance: #  q*d(mu_1,mu_2)
            print('Došlo k vytvoření nového shluku, důvodem je překročení podmínky \nMaxMin vzdálenost:'+str(max_hodnota_vsech_minim) + '> q*maxi_distance: ' + str(q*maxi_distance))
            pole_clusteru.append(Cluster(Point('x'+str(index_max), data.pop(index_max))))

        #   6.) V dalším kroku algorimu postupuje podobně jako v kroku 3, ale
        #  pro tři středy shluků. To znamená, že z minimálních vzdáleností
        #  obrazů trénovací množiny (T bez již použitých obrazů) ke všem středům
        #  co máme, najdeme maximalní. Pokud tato maximální vzdálenost je větší
        #  jak q-násobek průměru vzdáleností mezi již vytvoženými středy shluků
        #  vytvoříme nový shluk.
        else:
        #  7.) Pospup opakujeme pro vzrůstající počet (středů) shluků, dokud
        #  nová maximální vzdálenost nedosáhne do sporu s podmínkou pro
        #  vytvoření nového středu shluku (podmínka s q-násobkem)
            break
    # Po nalezení počtu shluků a jejich středů lze provést rozdělení obrazů
    #  trenovací množiny do jednotlivých shluků podle kriteria nejmenší
    #  vzdálenosti, přičemž za vzorové obrazy považujeme středy shluků mu_i
    for i in range(len(data)):
        nejmensi_vzdalenost = sys.maxsize
        index_nejmensi_vzdalenost = -1
        for j in range(len(pole_clusteru)):
            if fastest_calc_dist(data[i], pole_clusteru[j].get_center()) < nejmensi_vzdalenost:
                nejmensi_vzdalenost = fastest_calc_dist(data[i], pole_clusteru[j].get_center())
                index_nejmensi_vzdalenost = j
        pole_clusteru[index_nejmensi_vzdalenost].add_point(Point('x'+str(i)  ,data[i]))

    #  Výstup, print a podob
    print('Počet nalezených shluků je ' + str(len(pole_clusteru)))
    plt.axis(calculate_plot_space(data))

    for i in range(len(pole_clusteru)):
        plt.scatter(*zip(*pole_clusteru[i].get_points()),c=pole_clusteru[i].get_color(),marker='.' , s = 40, lw = 0)

    for i in range(len(pole_clusteru)):
        plt.scatter((*pole_clusteru[i].get_center()),c='r',marker='v' , s = 120, lw = 1)
        plt.annotate(
            str(i)+'. stred',
            xy = pole_clusteru[i].get_center(), xytext = (-20, 20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    plt.show()


if __name__ == "__main__":
    maxmin()
