# Určete počet tříd
# zkuste několik různých počátků
# TODO metody vzájemně porovnejte.

import sys
sys.path.append('../custom/')
from load import *
import matplotlib.pyplot as plt
import random


def retezova_mapa(data = load_data_file(), PRAH_VZDALENOSTI = 2.5):

    startovaci_bod = data.pop(random.randint(0, len(data)))
    # startovaci_bod = data.pop(320)
    serazena_data = []
    serazena_data.append(startovaci_bod)
    pocet_trid = 0

    highlight = []

    while (True):
        aktualni = serazena_data[-1] # aktualni neblizsi bod
        minimalni_vzdalenost = sys.maxsize
        kandidat = 0
        for i in range( len(data) ):    # Hledam sousda, tzn projet pole a najit nejblizsi bod se kterym sousedime.
            aktualni_vzdalenost = fastest_calc_dist(aktualni, data[i])
            if aktualni_vzdalenost < minimalni_vzdalenost:
                minimalni_vzdalenost = aktualni_vzdalenost
                kandidat = i
        else: # 'else' se vykona pouze pokud probehl for bez 'break'.
            if minimalni_vzdalenost > PRAH_VZDALENOSTI:
                pocet_trid += 1
                highlight.append(data[kandidat])
            serazena_data.append(data.pop(kandidat))

        if not data: # Pokud je list data prazdny, ukoncime.
            break

    # print(highlight)
    print('Počet tříd je ' + str(pocet_trid) + '\n \n práh, zvolená míra podobnosti je ' + str(PRAH_VZDALENOSTI))
    plt.axis(calculate_plot_space(serazena_data))
    plt.plot(*zip(*serazena_data))
    plt.scatter(*zip(*serazena_data), marker='.' , s = 40, lw = 0)
    for i in range(len(highlight)):
        plt.annotate(
            str(i)+'. prekroceni',
            xy = highlight[i], xytext = (-20, 20),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

    plt.show()


if __name__ == "__main__":
    retezova_mapa()
