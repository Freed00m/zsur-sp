import random
class Cluster:
    points = [] # here should be all poin objects
    center = [] # here should be recalculated center
    color = '#000000'

    relevantni_primky = [] # zde se uloží relevantní přímky
    relevatni_masky = [] # zde se budou ukladat masky, resp generovat


    def __init__(self, points):
        if isinstance(points, list):
            self.points = points
            self.center = self.calculate_center()
        else:
            self.points = [points]
            self.center = points
        self.color = "#%06x" % random.randint(0, 0xFFFFFF)

        self.relevantni_primky = []
        self.relevatni_masky = []

    # umožní iterovat objekt cluster,
    def __getitem__(self,index):
        return self.points[index]
    def __setitem__(self,index,value):
        self.points[index] = value

    # Reprezentace dat, pro výpis.
    def __repr__(self):
        return str(self.points)
    def __str__(self):
        return str(self.points)
    def __len__(self):
        return len(self.points)

    def add_point(self, point):
        self.points.append(point)

    def pop(self, index):
        return self.points.pop(index)

    def get_points(self):
        return self.points

    def get_matrix(self):
        data_matrix = []
        for i in range(len(self.points)):
            data_matrix.append(self.points[i].get_position())
        return data_matrix

    def calculate_center(self):
        x ,y = 0, 0
        if len(self.points) == 0:
            return self.points
        for i in range(len(self.points)):
            x += self.points[i][0]
            y += self.points[i][1]
        else:
            self.center = [x/i, y/i]
            return self.center

    def get_center(self):
        return self.center

    def flush_all_points(self):
        self.points = []

    def get_color(self):
        return self.color
    def merge_with(self, cluster):
        # c = list(set(a + b)) Unique set of list, will ignore duplicit points
        # self.points = list(set(self.points + cluster.get_points()))
        self.points = cluster.get_points() + self.points

    def add_primka(self, q, centroid):
        self.relevantni_primky.append(q)
        x = self.points[0].get_position() # Veme první bod k testu přímky
        if x[1] - centroid[1] > (q[0] + q[1] * (x[0] - centroid[0] )) / - q[2]:
            self.relevatni_masky.append(1)
        else:
            self.relevatni_masky.append(0)

    def get_primky(self):
        return self.relevantni_primky

    def get_maska(self):
        return self.relevatni_masky