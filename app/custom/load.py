import os
import sys
import numpy as np
import math
import cluster
import matplotlib.pyplot as plt


def fastest_calc_dist(p1,p2, option = 'euclid'):
    if option == 'squared':
        return (p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2
    else:
        return math.sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2)

def fastest_calc_dist_cluster(cluster1, cluster2, option = 'euclid'):
    vzdalenost_temp = sys.maxsize
    minimal_distance_from_clusters = -1
    for i in range(len(cluster1)):
        for j in range(len(cluster2)):
            curr_range = fastest_calc_dist(cluster1[i], cluster2[j] , option)
            if curr_range < vzdalenost_temp:
                minimal_distance_from_clusters = curr_range
    return minimal_distance_from_clusters

def numpy_calc_dist(p1,p2):
    return np.linalg.norm(np.array(p1)-np.array(p2))

def load_data_file(data_path = '../source/data.txt'):
    f = open(data_path, 'r')
    content = f.read().splitlines()
    data = []
    for i in range(len(content)):
        curr = str(content[i]).split(' ')
        for i in range(len(curr)):
            curr[i] = float(curr[i])
        data.append(curr)
    return data # return list of binary lists of points

def create_range_matrix(data = load_data_file(), option='euclid'):
    range_matrix = np.zeros((len(data), len(data)))
    for i in range(len(range_matrix)):
        for j in range(i, len(range_matrix)):
            range_matrix[i][j] = fastest_calc_dist(data[i], data[j], option)
            range_matrix[j][i] = range_matrix[i][j]
    return range_matrix

def create_range_matrix_from_clusters(data, option='euclid'):
    if not isinstance(data[0], cluster.Cluster):
        return "Insert an aray of clusters!!"
    range_matrix = np.zeros((len(data), len(data)))
    for i in range(len(range_matrix)):
        for j in range(i, len(range_matrix)):
            range_matrix[i][j] = fastest_calc_dist_cluster(data[i], data[j], option)
            range_matrix[j][i] = range_matrix[i][j]
    return range_matrix

def translate_data(data = load_data_file() ,by_x = -100, by_y = 100):
    translated = []
    for i in range(len(data)):
        translated.append([ data[i][0]+ by_x , data[i][1]+ by_y   ])
    return translated

def calculate_plot_space(data = load_data_file(), offset_percent = 10):
    a = list(zip(*data))
    max_x, max_y, min_x, min_y = max(a[0]), max(a[1]), min(a[0]), min(a[1])
    off_x = (max_x - min_x) * offset_percent/100
    off_y = (max_y - min_y) * offset_percent/100
    margins = [min_x - off_x, max_x + off_x, min_y - off_y, max_y + off_y]
    return margins



if __name__ == "__main__":
    # print(create_range_matrix())
    data = translate_data()
    plt.plot(*zip(*data), marker='.', color='r', ls='')
    plt.axis(calculate_plot_space(data))
    print(calculate_plot_space(data))
    plt.show()
