class Point:
    name = 'x_'
    position = []
    u_p = []

    def __init__(self, name, position):
        self.name = name
        self.position = position

    # Reprezentuje data, printne se pouze pozice 'zatím'
    def __repr__(self):
        return str(self.name)
    def __str__(self):
        return str(self.name)

    # Umožnit iterování nad objektem point, podle indexu vrátí hodnotu
    def __getitem__(self,index):
        return self.position[index]
    def __setitem__(self,index,value):
        self.position[index] = value

    def get_position(self):
        return self.position

    def get_name(self):
        return self.name

    def set_u_p(self, new_u_p):
        self.u_p = new_u_p

    def get_u_p(self):
        return self.u_p
