# Na výsledné rozdělení dat do jednotlivých tříd z bodu 2 vyzkoušejte iterativní optimalizaci


import sys
sys.path.append('../custom/')
sys.path.append('../2_kmeans')
from load import *
import matplotlib.pyplot as plt
import random
from rozdelte_do_trid import k_means
from point import Point
from cluster import Cluster
import numpy as np
import math

    # print(pole_clusteru
    # Naším úkolem je provést změnu zařazení vektoru x takovou, která maximalizuje kriteriální funkci.
    # * Mějme tedy stejnou kriteriální funkci jako u k-means
    # * Mějme tedy rozdělení trénovací množiny T na T_1, T_2, ..., T_R kde R je počet tříd. Tomu odpovídají četnosti v jednotlivých třídách s_i a jejich střední hodnoty mu_i
    #  * přeřaďme obrazový vektor ^x ze shluku i do shluku j
    # platí tedy ....
    #  s_i(k + 1) = s_i(k) - 1
    #  s_j(k + 1) = s_j(k) + 1

def iterativni_optimalizace(data = load_data_file(), debug = False):

    #  1.) Proveď počáteční rozklad N obrazů do R shluků, vypočti hodnotu kriteriální funkce J a urči mu_i pro i=1,...,R
    #
    [pole_clusteru, vychozi_jakost] = k_means(data)
    #  2.) Systematicky projeď všechny obrazy ^x
    #
    pole_jakosti = [sys.maxsize]
    celkova_jakost = vychozi_jakost
    broken = False
    migration = [] # pole polí, kde se ukládjí migrace, [T1 , x52 ->, T2]
    while(True):
        #  3.) ^x ∈ T_i, a s_i = 1 tak jdi na krok 6
        for i in range(len(pole_clusteru)):         #  i projíždí clustery
            for j in range(len(pole_clusteru[i])):  #  j projíždí každý itý bod
                #  4.) Vypočti
                #      A_i = s_i(k) / (s_i(k) - 1)  d^2 [ ^x , mu_i(k) ]
                #      A_j = s_j(k) / (s_j(k) + 1)  d^2 [ ^x , mu_j(k) ] ∀ i ≠ j
                #           a urči
                #      j* = argmax A_j
                #      Pokud A_i >  A_j*, tak přesuň ^x do T_j*
                #      Pokud A_i <= A_j* tak nic
                curr_bod = pole_clusteru[i][j].get_position()
                # print(len(pole_clusteru[i]))
                mu_i = pole_clusteru[i].get_center()
                s_i = len(pole_clusteru[i])
                A_i = (s_i / ( s_i - 1)) * fastest_calc_dist(curr_bod , mu_i,'squared')
                all_A_j = []
                # Je třeba projet všechny clustery a vybrat maximum
                A_j = -1
                index_A_j = -1
                for k in range(len(pole_clusteru)):
                    if i == k:
                        pass
                    else:
                        mu_j = pole_clusteru[k].get_center()
                        s_j = len(pole_clusteru)
                        curr_aj = (s_j / ( s_j + 1)) * fastest_calc_dist(curr_bod, mu_j,'squared')
                        if A_j < curr_aj:
                            A_j = curr_aj
                            index_A_j = k
                if j == 100 and debug:
                    debug = False
                    A_j = 0.122
                    print('------ \n A_i = '+ str(A_i) + '\n A_j = ' + str(A_j) )
                # print('------ \n A_i = '+ str(A_i) + '\n A_j = ' + str(A_j) )
                if A_i > A_j: # pokud se tak stane, popni bod a připoj ho jinam
                    migration.append([i, j, index_A_j])
                    print(migration)

        # Proveď přeházení bodů
        if migration:
            for _ in range(len(migration)):
                z_cluster, bod, kam_cluster = migration.pop(-1)
                print('--------\nDošlo k optimalizaci \n bod: '+str(pole_clusteru[z_cluster][bod])+' z clusteru T'+str(z_cluster)+' do clusteru T'+str(kam_cluster))
                pole_clusteru[kam_cluster].add_point(pole_clusteru[z_cluster].pop(bod))


        #  5.) Aktualizuj J, mu_i a mu_j
        for i in range(len(pole_clusteru)): # nejprve aktualizuj středy
            pole_clusteru[i].calculate_center()

        for j in range(len(pole_clusteru)): # pak až Jakosti
            jakost_jednoho_clusteru = -1
            for i in range(len(pole_clusteru[j])):
                jakost_jednoho_clusteru += fastest_calc_dist(pole_clusteru[j][i].get_position(), pole_clusteru[j].get_center())
            celkova_jakost += jakost_jednoho_clusteru

        #  6.) Jestliže se J nezměnila po pokusu přesunout postupně všech N obrazů trénovací množiny, proceduru ukonči. Jinak přejdi do bodu 2.

        if round(vychozi_jakost, 4) == round(celkova_jakost, 4):
            break
        else:
            vychozi_jakost = celkova_jakost
            celkova_jakost = 0

if __name__ == "__main__":
    # for i in range(100):
    #     print('\nPokus číslo'+str(i))
    #     iterativni_optimalizace()
    #     print('---------------------------------------------')

    iterativni_optimalizace()
