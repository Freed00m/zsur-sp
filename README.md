Semestrální práce na předmět "Základy strojového učení a rozpoznávání"


Tento projekt je napsán v jazyce python v3.5

Je dále třeba nainstalovat podporu například "OpenBlas" nutná pro funkčnost numpy.

V linuxu je často možné použít balík cblas lib, nebo openblaslapack.

Nutné dependence (instaluj třeba přes pip)

* cairocffi
* matplotlib
* numpy
* scipy